@login
Feature: Login

  @p1 @positive
  Scenario: Successful login
    Given I am on 'Login Page'
    When I logged in with credentials
      | username              | password |
      | oznurbakan@gmail.com  | oznur123 |
    Then 'Home Page' should be displayed

  @p2 @positive
  Scenario: Successful logout
    Given I have successfully logged in with credentials
      | username              | password |
      | oznurbakan@gmail.com  | oznur123 |
    When I click to Logout button
    Then 'Home Page without login button' should be displayed

  @p3 @negative
  Scenario Outline: '<case>' input fields checking on 'Login Page'
    Given I am on 'Login Page'
    When I type '<username>' as username and '<password>' as password
    Then '<errorMessageUserName>' '<errorMessagePassword>' error message should be displayed
    Examples:
      | case                        | username             | password          | errorMessageUserName                 | errorMessagePassword        |
      | Empty username and password |                      |                   | Bitte gib Deinen Benutzernamen ein   | Bitte gib Dein Passwort ein |

  @p3 @negative
  Scenario Outline: '<case>' input fields checking on 'Login Page'
    Given I am on 'Login Page'
    When I type '<username>' as username and '<password>' as password
    Then '<errorMessage>' error message must be displayed
    Examples:
      | case                        | username             | password             | errorMessage                                            |
      | Wrong username and password | xyz                  | oznurbakan@gmail.com | Entweder dein Nutzername oder das Passwort stimmt nicht.|
      | Empty username              |                      | oznur123             | Entweder dein Nutzername oder das Passwort stimmt nicht.|
      | Wrong username and password | xyz                  | oznurbakan@gmail.com | Entweder dein Nutzername oder das Passwort stimmt nicht.|
      | Empty password              | oznurbakan@gmail.com |                      | Entweder dein Nutzername oder das Passwort stimmt nicht.|