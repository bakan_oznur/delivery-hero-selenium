@amazon
Feature: Amazon

  @p1 @positive
  Scenario Outline: Search for '<case>'
    Given I am on amazon
    When I search for '<product>'
    Then 'Search Result' page should be displayed
    Examples:
      | case              | product      |
      | electronic type   | alexa        |
      | house type        | brush        |
      | specific product  | Kasa Smart Wi-Fi Plug by TP-Link - Control your Devices from Anywhere, No Hub Required, Works with Alexa and Google Assistant (HS100)  |