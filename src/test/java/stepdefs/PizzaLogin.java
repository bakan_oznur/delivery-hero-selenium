package stepdefs;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import modules.PizzaLogoutAction;
import modules.PizzaLoginAction;
import modules.PizzaLoginGenericAction;
import modules.PizzaLoginTestData;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import pages.PizzaHomePage;
import pages.PizzaLoginPage;
import java.util.List;
import static org.junit.Assert.assertTrue;

public class PizzaLogin {
    public WebDriver driver;

    public PizzaLogin() {
        driver = Hooks.driver;
    }

    @Given("^I am on 'Login Page'$")
    public void given_i_am_on_login_page() throws Throwable {
        driver.get("https://www.pizza.de");
    }

    @Then("^'Home Page' should be displayed$")
    public void then_home_page_should_be_displayed() throws Throwable {
        PageFactory.initElements(driver, PizzaHomePage.class);
    }

    @When("^I logged in with credentials$")
    public void when_i_logged_in_with_credentials(List<PizzaLoginTestData> loginTestDataList) throws Throwable {
        PageFactory.initElements(driver, PizzaLoginPage.class);
        PizzaLoginAction.Login(driver, loginTestDataList);
    }

    @Given("^I have successfully logged in with credentials$")
    public void given_i_have_successfully_logged_in_with_credentials(List<PizzaLoginTestData> loginTestDataList) throws Throwable {
        given_i_am_on_login_page();
        when_i_logged_in_with_credentials(loginTestDataList);
    }

    @When("^I click to Logout button$")
    public void when_i_click_to_logout_button() throws Throwable {
        PageFactory.initElements(driver, PizzaHomePage.class);
        PizzaLogoutAction.Logout(driver);
    }

    @Then("^'Home Page without login button' should be displayed$")
    public void then_home_page_without_login_button_should_be_displayed() throws Throwable {
        PageFactory.initElements(driver, PizzaHomePage.class);
        assertTrue(PizzaHomePage.isInitialized());
    }

    @When("^I type '(.*)' as username and '(.*)' as password$")
    public void when_i_type_x_as_username_and_y_as_password(String username, String password) throws Throwable {
        PageFactory.initElements(driver, PizzaLoginPage.class);
        PizzaLoginGenericAction.Login(driver, username, password);
    }

    @Then("^'(.*)' '(.*)' error message should be displayed$")
    public void then_errorMessageName_errorMessagePassword_error_message_should_be_displayed(String errorMessageUserName, String errorMessagePassword) throws Throwable {
        PageFactory.initElements(driver, PizzaLoginPage.class);
        PizzaLoginPage.displayErrorMessage(errorMessageUserName, errorMessagePassword);
    }

    @Then("^'(.*)' error message must be displayed$")
    public void then_x_error_message_must_be_displayed(String errorMessage) throws Throwable {
        PageFactory.initElements(driver, PizzaLoginPage.class);
        PizzaLoginPage.displayErrorMessage(errorMessage);
    }
}