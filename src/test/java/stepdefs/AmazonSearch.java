package stepdefs;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import modules.AmazonSearchAction;
import modules.AmazonSearchResultAction;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import pages.AmazonLandingPage;
import pages.AmazonSearchResultPage;

public class AmazonSearch {
    public WebDriver driver;

    public AmazonSearch()
    {
        driver = Hooks.driver;
    }

    @Given("^I am on amazon$")
    public void given_i_am_on_amazon() throws Throwable {
        driver.get("http://amazon.com");
    }

    @When("^I search for '(.*)'$")
    public void when_i_search_for_product(String product) throws Throwable {
        PageFactory.initElements(driver, AmazonLandingPage.class);
        AmazonSearchAction.Search(driver, product);
    }

    @Then("^'Search Result' page should be displayed$")
    public void then_search_result_page_should_be_displayed() throws Throwable {
        PageFactory.initElements(driver, AmazonSearchResultPage.class);
        AmazonSearchResultAction.SearchResult(driver);
    }
}