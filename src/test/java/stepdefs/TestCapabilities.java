package stepdefs;

import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import java.util.logging.Level;

import static base.Property.BROWSER_NAME;
import static base.Property.SELENIUM_LOG;

public class TestCapabilities {

    private DesiredCapabilities desiredCapabilities;
    private LoggingPreferences loggingPreferences;

    DesiredCapabilities getDesiredCapabilities() {

        //setting capabilities for initiating a WebDriver object
        loggingPreferences = new LoggingPreferences();
        loggingPreferences.enable(LogType.BROWSER, Level.parse(SELENIUM_LOG.toString()));
        loggingPreferences.enable(LogType.DRIVER, Level.parse(SELENIUM_LOG.toString()));

        desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setBrowserName(BROWSER_NAME.toString());
        desiredCapabilities.setCapability(CapabilityType.LOGGING_PREFS, loggingPreferences);

        return desiredCapabilities;
    }
}