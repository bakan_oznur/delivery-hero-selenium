package stepdefs;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import io.github.bonigarcia.wdm.InternetExplorerDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;
import java.net.MalformedURLException;
import static base.Property.BROWSER_NAME;

public class Hooks{

    public static WebDriver driver;
    private TestCapabilities testCapabilities = new TestCapabilities();

    enum Browsers{
        chrome, firefox, safari, ie;
    }

    @Before
    public void openBrowser() throws MalformedURLException {

        //opening browser based on desired test capabilities, chrome browser will be used by default
        Browsers browsers = Browsers.valueOf(BROWSER_NAME.toString().toLowerCase());
        switch (browsers) {
            case chrome:
                ChromeDriverManager.getInstance().setup();
                driver = new ChromeDriver(testCapabilities.getDesiredCapabilities());
                break;
            case firefox:
                FirefoxDriverManager.getInstance().setup();
                driver = new FirefoxDriver(testCapabilities.getDesiredCapabilities());
                break;
            case safari:
                driver = new SafariDriver(testCapabilities.getDesiredCapabilities());
                break;
            case ie:
                InternetExplorerDriverManager.getInstance().setup();
                driver = new InternetExplorerDriver(testCapabilities.getDesiredCapabilities());
                break;
            default:
                ChromeDriverManager.getInstance().setup();
                driver = new ChromeDriver(testCapabilities.getDesiredCapabilities());
                break;
        }
    }

    @After
    public void driverQuit(Scenario scenario) {
        driver.quit();
    }
}