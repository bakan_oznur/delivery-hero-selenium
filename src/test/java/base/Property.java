package base;

import static java.util.Optional.ofNullable;
import static org.junit.Assert.fail;

public enum Property {

    // defining properties for overriding default run settings
    BROWSER_NAME(ofNullable(System.getProperty("browser.name")).orElse("chrome")),
    SELENIUM_LOG(ofNullable(System.getProperty("selenium.log")).orElse("WARNING"));

    private String value;

    Property(String value) {
        this.value = value;
    }

    public String toString() {
        if (stringIsEmpty(value)) {
            fail("Property " + this.name() + " is missing.");
        }
        return value;
    }
    public static boolean stringIsEmpty(CharSequence cs) {
        return cs == null || cs.length() == 0;
    }
}