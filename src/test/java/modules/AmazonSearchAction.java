package modules;

import org.openqa.selenium.WebDriver;
import pages.AmazonLandingPage;
import static org.junit.Assert.assertTrue;

public class AmazonSearchAction {

    public static void Search(WebDriver driver, String product) throws Exception {
        //action items performed on landing page

        if (AmazonLandingPage.isInitialized()){ //verify search input field and search button visibility
            AmazonLandingPage.searchBar.click();
            AmazonLandingPage.searchBar.sendKeys(product);
            AmazonLandingPage.searchSubmit.click();
            assertTrue(true);
        }
        else
            assertTrue(false);
    }
}