package modules;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pages.AmazonSearchResultPage;
import java.util.List;

public class AmazonSearchResultAction {

    public static void SearchResult(WebDriver driver) throws Exception {

        if (AmazonSearchResultPage.searchResultList.size() > 0) {

            List<WebElement> product = AmazonSearchResultPage.searchResultItem.get(0).findElements(By.tagName("li"));

            //check that every item on the list should have a badge
            for (int column = 0; column < product.size(); column++) {
                    String text = product.get(column).getAttribute("data-asin");

                    if(product.get(column).findElement(By.tagName("span")).getAttribute("id").contains("AMAZONS_CHOICE"))
                    {
                        Assert.assertTrue(product.get(column).findElement(By.id("AMAZONS_CHOICE_" + text)).isDisplayed());
                    }
                    else if(product.get(column).findElement(By.tagName("span")).getAttribute("id").contains("BESTSELLER_"))
                    {
                        Assert.assertTrue(product.get(column).findElement(By.id("BESTSELLER_" + text)).isDisplayed());
                    }
                    else if(product.get(column).findElement(By.tagName("span")).getAttribute("id").contains("_"))
                    {
                        Assert.assertTrue(product.get(column).findElement(By.id(text)).isDisplayed());
                    }
            }

            //check that every item on the list should have an image
            for (int column = 0; column < product.size(); column++) {
                if(product.get(column).findElement(By.tagName("img")).isDisplayed())
                {
                    Assert.assertTrue(product.get(column).findElement(By.tagName("img")).isDisplayed());
                }
            }
        }
        Assert.assertFalse("Your search did not match with any products.", AmazonSearchResultPage.searchResultItem.size()<=0);
    }
}