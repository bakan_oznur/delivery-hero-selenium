package modules;

import org.openqa.selenium.WebDriver;
import pages.PizzaLoginPage;

public class PizzaLoginGenericAction {

    public static void Login(WebDriver driver, String username, String password) throws Exception{

        PizzaLoginPage.loginButton.click();
        Thread.sleep(1000);
        PizzaLoginPage.usernameTextBox.sendKeys(username);
        PizzaLoginPage.passwordTextBox.sendKeys(password);
        PizzaLoginPage.loginFormButton.click();
        Thread.sleep(1000);
    }
}