package modules;

import org.openqa.selenium.WebDriver;
import pages.PizzaHomePage;

public class PizzaLogoutAction {

    public static void Logout(WebDriver driver) throws Exception{

        PizzaHomePage.logoutButton.click();
        Thread.sleep(1000);
    }
}