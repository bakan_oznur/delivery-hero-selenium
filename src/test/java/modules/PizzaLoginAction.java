package modules;

import org.openqa.selenium.WebDriver;
import pages.PizzaLoginPage;
import java.util.List;

public class PizzaLoginAction {

    public static void Login(WebDriver driver, List<PizzaLoginTestData> loginTestDataList) throws Exception{

        PizzaLoginPage.loginButton.click();
        Thread.sleep(1000);
        PizzaLoginPage.usernameTextBox.sendKeys(loginTestDataList.get(0).getUsername());
        PizzaLoginPage.passwordTextBox.sendKeys(loginTestDataList.get(0).getPassword());
        PizzaLoginPage.loginFormButton.click();
        Thread.sleep(1000);
    }
}