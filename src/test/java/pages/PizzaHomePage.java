package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PizzaHomePage extends BaseClass{

    //Locators
    @FindBy(xpath = "//*[@data-qa='logout-button']")
    public static WebElement logoutButton;

    //Constructor
    public PizzaHomePage(WebDriver driver){
        super(driver);
    }

    public static boolean isInitialized() {
        if (PizzaLoginPage.loginButton.isDisplayed()) {
            return true;
        }
        return false;
    }

}