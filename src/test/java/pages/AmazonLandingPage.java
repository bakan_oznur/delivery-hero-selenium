package pages;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AmazonLandingPage extends BaseClass{

    //Landing page locators
    @FindBy(id = "twotabsearchtextbox")
    public static WebElement searchBar;
    @FindBy(className = "nav-input")
    public static WebElement searchSubmit;

    //Constructor
    public AmazonLandingPage(WebDriver driver){
        super(driver);
    }

    public static boolean isInitialized() {
        if (AmazonLandingPage.searchBar.isDisplayed() && AmazonLandingPage.searchSubmit.isDisplayed()) {
            return true;
        }
        return false;
    }
}