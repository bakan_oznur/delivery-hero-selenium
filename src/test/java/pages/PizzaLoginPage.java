package pages;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PizzaLoginPage extends BaseClass{

    //Locators
    @FindBy(xpath = "//*[@data-qa='login-button']")
    public static WebElement loginButton;

    @FindBy(xpath = "//*[@name='username']")
    public static WebElement usernameTextBox;

    @FindBy(xpath = "//*[@data-qa='login-form']//div[@class='form__row']//*[@name='password']")
    public static WebElement passwordTextBox;

    @FindBy(xpath = "//*[@data-qa='login-form-button-submit']")
    public static WebElement loginFormButton;

    @FindBy(xpath = "//*[@data-qa='login-form']//div[@class='form__row']//*[@class='input__error']")
    public static WebElement userNameErrorLabel;

    @FindBy(xpath = "//*[@data-qa='login-form']//div[@class='form__row'][2]//*[@class='input__error']")
    public static WebElement passwordErrorLabel;

    @FindBy(xpath = "//*[@data-qa='user-form-message-error']")
    public static WebElement generalErrorLabel;

    //Constructor
    public PizzaLoginPage(WebDriver driver){
        super(driver);
    }
    public static void displayErrorMessage(String errorMessageUserName, String errorMessagePassword) {

        if((errorMessageUserName.isEmpty() && errorMessagePassword.isEmpty()))
        {
            Assert.assertEquals(errorMessageUserName, userNameErrorLabel.getText());
            Assert.assertEquals(errorMessagePassword, passwordErrorLabel.getText());
        }
        else if (errorMessageUserName.isEmpty())
        {
            Assert.assertEquals(errorMessageUserName, userNameErrorLabel.getText());
        }
        else if (errorMessagePassword.isEmpty())
        {
            Assert.assertEquals(errorMessagePassword, passwordErrorLabel.getText());
        }
    }

    public static void displayErrorMessage(String errorMessage) {
        Assert.assertEquals(errorMessage, generalErrorLabel.getText());
    }
}