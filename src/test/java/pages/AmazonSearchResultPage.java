package pages;

import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AmazonSearchResultPage extends BaseClass{

    //Search result list locators
    @FindBy(xpath = "//*[contains(@id,'result_')]")
    public static List<WebElement> searchResultList;

    //Search result item locators
    @FindBy(id = "atfResults")
    public static List<WebElement> searchResultItem;

    //Constructor
    public AmazonSearchResultPage(WebDriver driver){
        super(driver);
    }
}