delivery-hero-selenium
=====================

oznur bakan assignment for delivery hero senior automation engineer position

# Prerequisites
if you're using osx or linux you should install maven with following command
    brew install maven node
if you're using windows you should use following command
    scoop install maven node
    
# Technology used
Selenium
Maven
Cucumber

# Notes
Page object pattern is used
Behaviour driven developed
Test data is managed over feature files, it eases the data maintenance

# You can run test cases from terminal using following commands
by default cucumber executes the whole scenarios if test cases are executed using RunCukesTest class
    mvn clean -D"browser.name=chrome" test
    mvn clean -D"browser.name=firefox" test
    mvn clean -D"browser.name=safari" test

# Test report can be displayed under target/cucumber-html-report